from requests import get
from warnings import catch_warnings, simplefilter
from bs4 import BeautifulSoup
from markdownify import markdownify
from news_aggregator.interfaces import NewsAggregator

class LegibleNews(NewsAggregator):
    url = 'https://legiblenews.com'

    def fetch(self):
        with catch_warnings():
            simplefilter("ignore")
            try:
                data = get(self.url, verify=False)
            except ConnectionError:
                raise ConnectionError('Cannot connect to Legible News!')
            if data.status_code != 200:
                raise ConnectionError('Legible News isn\'t responding!')
            else:
                try:
                    return BeautifulSoup(data.text, 'html.parser')
                except:
                    raise UnicodeTranslateError('Unreadable HTML Document')

    def parse_date(self, data):
        try:
            title_text = data.head.title.text.split('•')[0]
            return title_text
        except:
            raise UnicodeTranslateError('Legible News heading unreadable!')
        
    def generate_title(self, data):
        return '# News Briefing for ' + self.parse_date(data) + '\n\n'

    def parse_body(self, data):
        try:
            dirty_body = data.body.article
            for div in dirty_body.find_all("div", {'class': "mt-24 mb-20"}):
                div.decompose()
            body_markdown = markdownify(str(dirty_body), heading_style='ATX')
            return self.generate_title(data) + body_markdown
        except:
            raise UnicodeTranslateError('Legible News heading unreadable!')
    
    def get_text(self):
        data = self.fetch()
        return self.generate_title(data), self.parse_body(data)