from pytest import fixture
from bs4 import BeautifulSoup
from news_aggregator.parsers import LegibleNews

@fixture
def parser():
    return LegibleNews()

def test_fetch(parser):
    html_data = parser.fetch()
    assert bool(html_data.find())

def test_title_generation(parser):
    html_data = parser.fetch()
    assert isinstance(parser.generate_title(html_data), str)

def test_parse(parser):
    _, body = parser.get_text()
    assert not(bool(BeautifulSoup(body, 'html.parser').find()))